// 1. Циклы помогают выполнять некоторые участки кода n количество раз. На примере цикла for у нас есть как бы (инициализация счётчика; условие цикла; как менять счетчик с каждым разом). Это нам помогает избежать в коде множество строчек с одинаковыми операциями.

let userNumber;

do {
    userNumber = prompt('Введите число');
} while (!userNumber || userNumber.trim() === "" || isNaN(userNumber));

if (userNumber < 5) {
    console.log('Sorry, no numbers');
} else {
    for (i = 0; i <= userNumber; i++) {
        if (i % 5 == 0)
        console.log(i);
    }
}